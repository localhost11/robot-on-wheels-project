# Markdown APP side
### _edition 1._
![N|Solid](https://www.hva.nl/binaries/content/gallery/hva/over-de-hva/huisstijl/logo-hva.png)
## Introductie
In deze readme wordt uitgelegd wat ons project inhoudt, wat het doet en wat de voordelen van ons idee zijn. Ons team bestaat uit 5 leden.
- [REDACTED]
- Diego Brandjes
- [REDACTED]
- [REDACTED]
- [REDACTED]

## Ons project
_**De opdracht**_
Tijdens dit project is het de bedoeling dat we als team een robotcar ontwikkelen, met als doel om mensen te redden of helpen. Hierbij moet gebruik gemaakt worden van een mobiel apparaat die de robot aan kan sturen. Ook gebruiken we een camera op de auto die uitgelezen kan worden via een app. Via deze camera worden ook bepaalde objecten gedetecteerd.

_**Onze toepassing**_
Ons idee is het maken van een robotcar die puin kan ruimen in gebieden waar bijvoorbeeld gebouwen zijn ingestort of in berggebieden waar ook veel brokstukken liggen. Onze auto is ook voorzien van propellers die blaadjes en andere licht puin weg kan blazen. Voor de grotere brokstukken hebben we gebruik gemaakt van een borstel die draait, op deze manier wordt de weg vrijgemaakt. Ook hebben we een camera op de auto bevestigd die video kan doorsturen naar onze app. 

_**Waarom is dit nuttig?**_
In de toekomst gaan robots een steeds grotere rol spelen. Dit project laat ons nadenken over de toepassingen van robots in noodsituaties. Een robot kan handig helpen bij gevaarlijke acties, zo kan kunnen mensenlevens bespaard worden. Via een camera kunnen kleine ruimtes worden bekeken, waar een robot wel in past, maar een mens niet.
