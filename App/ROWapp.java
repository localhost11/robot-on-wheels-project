package com.example.myfirstapplication;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import java.io.OutputStream;
import java.net.Socket;


public class MainActivity extends AppCompatActivity {

    // Initialising values such as port values and setting the toggles. 
    int PORT = 8080;
    String IP;
    boolean b_toggle = true;
    boolean f_toggle = true;


    WebView webView;

    /*
        This function runs on startup and sets the views correctly.
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button W = findViewById(R.id.buttonW);
        Button A = findViewById(R.id.buttonA);
        Button S = findViewById(R.id.buttonS);
        Button D = findViewById(R.id.buttonD);
        Button B = findViewById(R.id.buttonB);
        Button F = findViewById(R.id.buttonF);
        View web = findViewById(R.id.webview);

        web.setVisibility(View.INVISIBLE);
        W.setVisibility(View.INVISIBLE);
        A.setVisibility(View.INVISIBLE);
        S.setVisibility(View.INVISIBLE);
        D.setVisibility(View.INVISIBLE);
        B.setVisibility(View.INVISIBLE);
        F.setVisibility(View.INVISIBLE);
    }

    /*
        This function uses a textfield to let the user input values 
        which are then put into the variable IP. The view is also changed by setting views to visible.
    */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void setIP (View v) throws Exception{

        EditText ipEditText = (EditText) findViewById(R.id.IP);
        IP = ipEditText.getText().toString();

        Button W = findViewById(R.id.buttonW);
        Button A = findViewById(R.id.buttonA);
        Button S = findViewById(R.id.buttonS);
        Button D = findViewById(R.id.buttonD);
        Button B = findViewById(R.id.buttonB);
        Button F = findViewById(R.id.buttonF);
        Button O = findViewById(R.id.buttonB2);
        View web = findViewById(R.id.webview);
        TextView text = findViewById(R.id.IP);

        web.setVisibility(View.VISIBLE);
        W.setVisibility(View.VISIBLE);
        A.setVisibility(View.VISIBLE);
        S.setVisibility(View.VISIBLE);
        D.setVisibility(View.VISIBLE);
        B.setVisibility(View.VISIBLE);
        F.setVisibility(View.VISIBLE);

        text.setVisibility(View.INVISIBLE);
        O.setVisibility(View.INVISIBLE);

        // Starting of webview using the IP variable, sets multiple scripts to be used and opens a new client.
        WebView webview = (WebView) findViewById(R.id.webview);
        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webview.getSettings().setPluginState(WebSettings.PluginState.ON);
        webview.getSettings().setMediaPlaybackRequiresUserGesture(false);
        webview.setWebChromeClient(new WebChromeClient());
        webview.loadUrl("http://" + IP + ":5000");
        onWindowFocusChanged(true);

    }


    /* 
        Sends char messages via sockets, this is a multithreaded part 
        and will run once until it has send it's message.
    */
    public void send(char msg){
        new Thread(){
            public void run() {

                //while true is still here because it has not been tested without it yet, probably redundant.
                while (true) {
                    try {
                        /* 
                            when there is a message change a new socket will be opened up
                            and the message will be send, afterwards the socket will be closed.
                        */
                        if (msg != ' '){
                            Socket socket = new Socket(IP, PORT);
                            OutputStream output = socket.getOutputStream();

                            output.write(msg);
                            output.close();
                            socket.close();

                            break;
                        }

                    } catch (Exception e) {

                    }
                }
            }
        }.start();
    }

    // forces fullscreen by setting flags.
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }


    /*
        Functions below will change a variable when a button is pressed and held, upon release 
        the value is changed and send again, uses OnTouchListener's actions.
    */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void up (View v) throws Exception {

        v.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    send('w');
                }
                if (event.getAction() == MotionEvent.ACTION_UP){
                    send('p');
                }
                return true;
            }
        });
    }

    public void down (View v) throws Exception {
        v.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    send('s');
                }
                if (event.getAction() == MotionEvent.ACTION_UP){
                    send('p');
                }
                return true;
            }
        });
    }

    public void left (View v) throws Exception {
        v.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    send('a');
                }
                if (event.getAction() == MotionEvent.ACTION_UP){
                    send('p');
                }
                return true;
            }
        });
    }

    public void right (View v) throws Exception {
        v.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    send('d');
                }
                if (event.getAction() == MotionEvent.ACTION_UP){
                    send('p');
                }
                return true;
            }
        });
    }


    // Brush and Fans, operate on the toggle basis and flipflops between the messages sent.
    public void brush (View v) throws Exception {
        Button B = findViewById(R.id.buttonB);
        b_toggle = !b_toggle;
        if (!b_toggle){
            B.setText("On");
            send('b');
        }else{
            B.setText("Brush");
            send('n');
        }

    }

    public void fans (View v) throws Exception {
        Button F = findViewById(R.id.buttonF);
        f_toggle = !f_toggle;
        if (!f_toggle){
            F.setText("On");
            send('f');
        }else{
            F.setText("Fans");
            send('g');
        }
    }

}
