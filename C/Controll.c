// C code for motor control.

#include <stdio.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

// Defines values for pins, ports and directions.
#define PORT 8080
#define FORWARD 1
#define BACKWARDS 2
#define STOP 0
#define FANS 17
#define BRUSH 27    

int server_fd, new_socket;
struct sockaddr_in address;
int opt = 1;
int addrlen = sizeof(address);
char buffer[1] = {0};

/* char array, 7 is the size value, 3 is the speed, all the zeroes are 
   changed to modify the cars behaviour.
*/
unsigned char stuur[7] = {7, 3, 0, 0, 3, 0, 0};


/* drive function, needs direction values for both sides, see defined values.
   fd is the location of the I2C component. The stop function works the 
   same but the directions are set to 0 instead. Write will send the values of 
   the char array to the component. 
*/
void drive(int speed, int direction_l, int direction_r) {
    int fd = wiringPiI2CSetup(0x32);

    stuur[2] = speed;
    stuur[3] = direction_l;
    stuur[5] = speed;
    stuur[6] = direction_r;

    write(fd, &stuur[0], 7);
}

void stop() {
    int fd = wiringPiI2CSetup(0x32);

    stuur[2] = STOP;
    stuur[3] = STOP;
    stuur[5] = STOP;
    stuur[6] = STOP;

    write(fd, &stuur[0], 7);
}

void listener() {

     // Creating socket file descriptor and error message if it fails.
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    // Forcefully attaching socket to the port 8080
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                   &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    // Forcefully attaching socket to the port 8080
    if (bind(server_fd, (struct sockaddr *)&address,
             sizeof(address))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char const *argv[])
{
    // Assign value for while loop and initialize the listener.
    uint8_t loop = 1;
    listener();

    while(loop) {

        //send(new_socket, hello, strlen(hello), 0);

        if ((new_socket = accept(server_fd, (struct sockaddr *) &address, (socklen_t * ) & addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }             

        // read the buffer and write value to the buffer array, clears the buffer afterwards.
        read(new_socket, buffer, 1);
        close(new_socket);

        //Switchcase for controlling the car
        switch (buffer[0]) {

                /*Here we have the switch cases for controlling the car.
                First it prints whats happening in the consol.
                Then it will look what the directions are.*/
            case 'w':
                printf("Forward\n");
                drive(0x03, FORWARD, FORWARD);
                break;
            case 's':
                printf("Backward\n");
                drive(0x03, BACKWARDS,BACKWARDS);
                break;
            case 'd':
                printf("Right\n");
                drive(0x01, BACKWARDS, FORWARD);
                break;
            case 'a':
                printf("Left\n");
                drive(0x01, FORWARD, BACKWARDS);
                break;
            case 'q':
                loop = STOP;
                break;
            case 'p':
                stop();
                break;

                /*These are the brushes and fans cases. Everytime a one of these cases is called 
                the first thing it will do is print a line in the consol describing what happend.
                Then it sets it to SetupGpio. After that it will look what gpio pin it is and if its
                an output or input pin and activates or deactivates it. */
            case 'f':
                printf("Fans On\n");
                wiringPiSetupGpio();
                pinMode(FANS, OUTPUT);
                digitalWrite(FANS, HIGH);
                break;

            case 'g':
                printf("Fans Off\n"); 
                wiringPiSetupGpio();
                pinMode(FANS, OUTPUT);
                digitalWrite(FANS, LOW);                         
                break;

            case 'b':
                printf("Brush On\n");
                wiringPiSetupGpio();
                pinMode(BRUSH, OUTPUT);
                digitalWrite(BRUSH, HIGH);
                break;

            case 'n':
                printf("Brush Off\n");
                wiringPiSetupGpio();
                pinMode(BRUSH, OUTPUT);
                digitalWrite(BRUSH, LOW);
                break;

            default:
                loop = 1;
        }
        delay(10);
    }
    printf("Has run successfully\n");
    return 0;
}
