# This is a part of the main from a project from 2nd year 



# Markdown Project
### _edition 1._
![N|Solid]()
<img src="https://www.hva.nl/binaries/content/gallery/hva/over-de-hva/huisstijl/logo-hva.png" alt="hvalogo" width="200"/>
## Introductie
In deze readme wordt uitgelegd wat ons project inhoudt, wat het doet en wat de voordelen van ons idee zijn. Ons team bestaat uit 4 leden.
- [REDACTED] (Scrum master)
- Diego Brandjes (Product Owner)
- [REDACTED]
- [REDACTED]

Ook wordt er wat verteld over hoe ons project technisch werkt. Er zijn schematische tekeningen toegevoegd met bijhorende uitleg.

## Ons project
_**De opdracht**_
Tijdens dit project is het de bedoeling dat we als team een robotcar ontwikkelen, met als doel om mensen te redden of helpen. Hierbij moet gebruik gemaakt worden van een mobiel apparaat dat de robot aan kan sturen. Ook gebruiken we een camera op de auto die uitgelezen kan worden via een app. Er worden verschillende sensoren en actuatoren gebruikt die het helpen van mensen makkelijker maakt.

_**Onze toepassing**_
Ons idee is het maken van een robotcar die puin kan ruimen in gebieden waar bijvoorbeeld gebouwen zijn ingestort of in berggebieden waar ook veel brokstukken liggen. Onze auto is ook voorzien van propellers die blaadjes en andere licht puin weg kan blazen. Voor de grotere brokstukken hebben we gebruik gemaakt van een borstel die draait, op deze manier wordt de weg vrijgemaakt. Ook hebben we een camera op de auto bevestigd die video kan doorsturen naar onze app. Aan de achterkant van de auto is een ultrasone sensor bevestigd die uitgelezen kan worden om er voor te zorgen dat er niet tegen muren aangereden wordt.

_**Waarom is dit nuttig?**_
In de toekomst gaan robots een steeds grotere rol spelen. Dit project laat ons nadenken over de toepassingen van robots in noodsituaties. Een robot kan handig helpen bij gevaarlijke acties, zo kan kunnen mensenlevens bespaard worden. Via een camera kunnen kleine ruimtes worden bekeken, waar een robot wel in past, maar een mens niet.

## Hoe begin je met dit project?
Op onze digitale leeromgeving hebben we een handleiding over ons project ingeleverd. In deze handleidng staat hoe we te werk zijn gegaan en welke stappen we hebben gezet om ons project te voltooien. Als deze handleiding gevolgd wordt, dan zou men een volledig werkende hulp auto moeten kunnen maken.

## Documentatie
Op onze digitale leeromgeving hebben we een software beschrijving geplaatst waar alles over onze software instaat wat men moet weten over ons project. Er wordt precies uitgelegd wat de de functies zijn van welke stuk code.

## Onderdelen

<img src="https://cdn.discordapp.com/attachments/932574470319767562/932581854857535548/car_6.jpg" alt="Brush" width="250"/>

Borstel van de pi.

<img src= "https://cdn.discordapp.com/attachments/932574470319767562/932580750698639400/unknown.png" alt="Brush" width="250"/>

Propellers van de pi.

<img src="https://cdn.discordapp.com/attachments/932574470319767562/932581854035456040/car_4.jpg" alt="Ultrasone" width="250"/>

Ultrasonic sensor van de pi.

<img src= "https://cdn.discordapp.com/attachments/932574470319767562/932581853335015434/car_2.jpg" alt="Ultrasone" width="250"/>

Camera on the pi, controlled by using OpenCV.

## Schematische ontwerpen
Hieronder staat een aantal schematische ontwerpen die een goed inzicht geven van hoe ons product werkt.

<img src="https://cdn.discordapp.com/attachments/932574470319767562/932944332057636874/archdesignrow.png" alt="schemdrawrpi" width="1000"/>


De Raspberry Pi staat centraal in ons project. Via dit apparaatje wordt alles aangestuurd en wordt alles ontvangen.
De ultrasone sensor wordt ingelezen en er wordt naar geschreven om ermee te werken.
De camera wordt uitgelezen via opencv.
De borstel en de fans worden ook aangestuurd door de Pi en kunnen aan en uit gezet worden.
Via de mobile application wordt de dit alles aangestuurd.


<img src="https://cdn.discordapp.com/attachments/932574470319767562/932949569237573712/srf02schem.png" alt="schemdrawultrasonic" width="500"/>

De aansluiting van de ultrasone sensor (SRF02) is als volgt gerealiseerd.
- De 3.3v pin op de SRF02 is aangesloten op de 3.3v pin op de RPI.
- De SDA pin op de SRF02 is aangesloten op de GPIO2 pin op de RPI.
- De SCL pin op de SRF02 is aangesloten op de GPIO5 pin op de RPI.
- De Ground pin op de SRF02 is aangesloten op de Ground pin op de RPI.


<img src="https://cdn.discordapp.com/attachments/932574470319767562/932956543622713384/unknown.png" alt="schemdrawbrush" width="500"/>

Hier is een schematische tekening van de aansluiting van de borstel op de pi. Er is gebruik gemaakt 
van Mosfets, resistoren en diverse kabels.


<img src="https://cdn.discordapp.com/attachments/932574470319767562/932956730671923230/unknown.png" alt="schemdrawfans" width="500"/>

Hier is een schematische tekening van de aansluiting van de propellors op de pi. Er is gebruik gemaakt 
van Mosfets, resistoren en diverse kabels.

## Contact
Om contact op te nemen voor vragen of andere zaken zijn de volgende email adressen te bereiken:
- [REDACTED]
- [REDACTED]
- [REDACTED]
- Diego.Brandjes.@hva.nl

